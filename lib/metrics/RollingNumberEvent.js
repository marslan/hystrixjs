"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports["default"] = {
    SUCCESS: "SUCCESS",
    FAILURE: "FAILURE",
    TIMEOUT: "TIMEOUT",
    SHORT_CIRCUITED: "SHORT_CIRCUITED"
};
module.exports = exports["default"];